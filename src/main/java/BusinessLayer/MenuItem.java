package BusinessLayer;

public interface MenuItem {
    double ComputePrice();
    int getId();
    String getName();
}
