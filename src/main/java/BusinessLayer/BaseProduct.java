package BusinessLayer;

import java.io.Serializable;

public class BaseProduct implements MenuItem, Serializable {

    private int id;
    private String name;
    private double price;

    public BaseProduct(int id, String name, double price) {
        this.id=id;
        this.name=name;
        this.price=price;
    }

    public int getId() { return id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String toString() {
        return name + "........." + price +" RON";
    }

    public double ComputePrice(){
        this.setPrice(this.price);
        return this.price;
    }
}
