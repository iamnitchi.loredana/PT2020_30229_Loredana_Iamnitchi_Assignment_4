package DataLayer;
import BusinessLayer.Restaurant;
import java.io.*;

public class RestaurantSerializator implements Serializable {

    public static void Serialization(Restaurant r, String fileName) {
        try
        {
            FileOutputStream file = new FileOutputStream( fileName);
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(r);
            out.close();
            file.close();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
    }

    public static Restaurant Deserialization(String filename) {
        Restaurant restaurant=new Restaurant();
        try {
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);
            restaurant=(Restaurant)in.readObject();
            in.close();
            file.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return restaurant;
    }
}
