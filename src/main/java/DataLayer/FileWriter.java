package DataLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import java.io.PrintWriter;
import java.util.*;

public class FileWriter {

    public static void bill(HashMap<Order, ArrayList<MenuItem>> orders, int idOrder) {
        for (Map.Entry m : orders.entrySet()) {
            Object obj=m.getKey();
            Order order=(Order) obj;
            ArrayList<MenuItem> ord=orders.get(order);
            int id=order.getIdOrder();
            if(id==idOrder) {
                try {
                    PrintWriter bon = new PrintWriter("Bill for id " + order.getIdOrder() + ".txt");
                    bon.println("Order: " + order.getIdOrder());
                    bon.println("Table: " + order.getTable());
                    bon.println("Date: " + order.getDate());
                    double totalPrice = 0;
                    for (MenuItem product : ord) {
                        bon.println(product.toString());
                        totalPrice += product.ComputePrice();
                    }
                    bon.println("The total price for this order is " + totalPrice + " RON");
                    bon.close();
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }
}
