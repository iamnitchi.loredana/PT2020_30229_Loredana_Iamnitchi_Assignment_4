package PresentationLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import static DataLayer.RestaurantSerializator.Serialization;


public class AdministratorGUI{

    private JButton insertBP = new JButton("Insert BaseProduct");
    private JButton insertCP = new JButton("Insert Composite Product");
    private JButton delete = new JButton("Delete Product");
    private JButton update = new JButton("Update Product");
    private JButton show = new JButton("Show Menu");
    private JButton save = new JButton("Save");

    private JLabel labelId = new JLabel("Id:");
    private JLabel labelName = new JLabel("Name:");
    private JLabel labelPrice= new JLabel("Price:");
    private JLabel labelIngr= new JLabel("Ingredients:");

    private JTextField text1= new JTextField(5);
    private JTextField text2= new JTextField(15);
    private JTextField text3= new JTextField(5);
    private JTextField text4= new JTextField(25);
    private JTextField text5= new JTextField(25);

    private JTable table=new JTable();
    private Restaurant r;

    public AdministratorGUI(final Restaurant r) {
        this.r = r;
        JFrame administrator = new JFrame("Administrator");
        administrator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        administrator.setSize(500, 400);

        JPanel c1 = new JPanel();
        JPanel c2 = new JPanel();
        JPanel c3 = new JPanel();
        JPanel c4 = new JPanel();
        JPanel c5 = new JPanel();

        c1.add(labelId);
        c1.add(text1);
        c1.add(labelName);
        c1.add(text2);
        c1.add(labelPrice);
        c1.add(text3);
        c1.setLayout(new FlowLayout());

        c2.add(labelIngr);
        c2.add(text5);
        c2.setLayout(new FlowLayout());

        c3.add(insertBP);
        c3.add(insertCP);
        c3.add(delete);
        c3.add(update);
        c3.add(show);
        c3.add(save);
        c3.setLayout(new FlowLayout());

        insertBP.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
                double price;
                String sPrice=text3.getText();
                if(sPrice.equals(""))
                    price=0;
                else
                    price=Double.parseDouble(sPrice);
                String name=text2.getText();
                text4.setText(r.insertBaseProduct(name, price));
            }
        });

        insertCP.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
                int ok=0;
                String name=text2.getText();
                String t1=text5.getText();
                List<MenuItem> menu=r.getMenu();
                ArrayList<MenuItem> ingredients= new ArrayList<>();
                String []t=t1.split("\\, ");
                for (String prodName:t) {
                    for (MenuItem m : menu) {
                        if (m.getName().equals(prodName))
                            ingredients.add(m);
                    }
                }
                if(t.length==ingredients.size())  ok=1;
                if(ok==0 && ingredients.size()!=0)
                    text4.setText("The product is not in the menu");
                else
                    text4.setText(r.insertCompositeProduct(name, ingredients));
            }
        });

        update.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
                int ok=0;
                double price;
                int id;
                String sId=text1.getText();
                if(sId.equals("")) id=0;
                else id=Integer.parseInt(sId);
                String sPrice=text3.getText(); //price
                String t1=text5.getText(); //ingr
                List<MenuItem> menu=r.getMenu();
                ArrayList<MenuItem> ingredients= new ArrayList<>();
                if(t1.equals(""))
                    ingredients=null;
                else {
                    String []t=t1.split("\\, ");
                    for (String prodName:t) {
                        for (MenuItem m : menu) {
                            if (m.getName().equals(prodName))
                                ingredients.add(m);
                        }
                    }
                    if(t.length==ingredients.size())  ok=1;
                }
                if(sPrice.equals("")) price=0;
                else price=Double.parseDouble(sPrice);
                String name=text2.getText();
                if(ok==0 && ingredients!=null)
                    text4.setText("The ingredient is not in the menu");
                else {
                    r.updateProduct(id, name, price, ingredients);
                    text4.setText("Successful update");
                }
            }
        });

        delete.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
                String name=text2.getText();
                text4.setText(r.deleteProduct(name));
            }
        });

        show.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
                List<MenuItem> menu = r.getMenu();
                String[] columns= {"id","name","price"};
                String [][]data= new String[50][3];
                int indexMatrice=0;
                for(MenuItem a: menu) {
                        String[] rowElements = new String[3];
                        rowElements[0]=Integer.toString(a.getId());
                        rowElements[1]=a.getName();
                        rowElements[2]=Double.toString(a.ComputePrice());
                        data[indexMatrice]=rowElements;
                        indexMatrice++;
                }
                table = new JTable(data,columns);
                table.setCellSelectionEnabled(true);
                table.setBounds(300,400,500,400);
                JFrame f=new JFrame("Menu");
                f.add(new JScrollPane(table));
                f.setSize(500,400);
                f.setVisible(true);
            }
        });

        save.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
                r.existsOrders();
                Serialization(r, "restaurantAfter.ser");
            }
        });

        c4.add(text4);
        c4.setLayout(new FlowLayout());

        c5.add(c1);
        c5.add(c2);
        c5.setLayout(new FlowLayout());

        administrator.add(c5);
        administrator.add(c3);
        administrator.add(c4);
        administrator.setLayout(new GridLayout(0,1));
        administrator.setVisible(true);
    }
}
