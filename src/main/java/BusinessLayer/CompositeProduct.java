package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompositeProduct implements MenuItem, Serializable {
    private int id;
    private String name;
    private double price;
    private List<MenuItem> ingredients;

    public CompositeProduct(int id, String name) {
        this.id=id;
        this.name=name;
        this.ingredients= new ArrayList<>();
    }

    public int getId() { return id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<MenuItem> getIngredients() { return ingredients; }

    public void setIngredients(ArrayList<MenuItem> ingredients) {
        this.ingredients = ingredients;
    }

    public String toString() {
        return name + "........." + price + " RON" ;
    }

    public double ComputePrice(){
        double totalPrice=0;
        for(MenuItem m : ingredients)
            totalPrice+=m.ComputePrice();
        this.setPrice(totalPrice);
        return totalPrice;
    }
}
