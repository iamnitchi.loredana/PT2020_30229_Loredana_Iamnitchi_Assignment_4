package BusinessLayer;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Order implements Serializable {

    private int idOrder;
    private int table;
    private Date date;

    public Order(int idOrder, Date date, int table) {
        this.idOrder=idOrder;
        this.table = table;
        this.date = date;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public int getTable() {
        return table;
    }

    public Date getDate() {
        return date;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getIdOrder() == order.getIdOrder() &&
                getTable() == order.getTable() &&
                Objects.equals(getDate(), order.getDate());
    }

    public int hashCode() {
        return Objects.hash(getIdOrder(), getTable(), getDate());
    }

    public String toString() {
        return "idOrder=" + idOrder + ", table=" + table + ", date=" + date;
    }
}
