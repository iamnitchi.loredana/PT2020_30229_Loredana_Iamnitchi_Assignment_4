package PresentationLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;


public class WaiterGUI {

    private JButton createOrder = new JButton("Create Order");
    private JButton generateBill = new JButton("Generate Bill");
    private JButton showOrders= new JButton("Show Orders");
    private JButton showMenu = new JButton("Show Menu");

    private JLabel labelId = new JLabel("Id:");
    private JLabel labelTable = new JLabel("Table:");
    private JLabel labelProducts= new JLabel("Ordered Products:");

    private JTextField text1= new JTextField(5);
    private JTextField text2= new JTextField(5);
    private JTextField text3= new JTextField(25);
    private JTextField text4= new JTextField(25);

    private DateFormat dateFormat;
    private JTable table=new JTable();
    private Restaurant r;
    private static int idO=1;

    public WaiterGUI(final Restaurant r){
        this.r = r;
        this.dateFormat=new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        JFrame administrator = new JFrame("Waiter");
        administrator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        administrator.setSize(500, 400);

        JPanel c1 = new JPanel();
        JPanel c2 = new JPanel();
        JPanel c3 = new JPanel();
        JPanel c4 = new JPanel();
        JPanel c5 = new JPanel();

        c1.add(labelId);
        c1.add(text1);
        c1.add(labelTable);
        c1.add(text2);
        c1.setLayout(new FlowLayout());

        c2.add(labelProducts);
        c2.add(text3);
        c2.setLayout(new FlowLayout());

        c3.add(createOrder);
        c3.add(generateBill);
        c3.add(showMenu);
        c3.add(showOrders);
        c2.setLayout(new FlowLayout());

        createOrder.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
                int id=idO, table;
                idO++; Order o;
                List<MenuItem> menu=r.getMenu();
                String sTable=text2.getText();
                if(sTable.equals("") || sTable.equals("0"))
                    o = null;
                else {
                    table = Integer.parseInt(sTable);
                    o = new Order(id, new Date(), table);
                }
                String t1=text3.getText(); //ordered products
                ArrayList<MenuItem> orderedProducts=new ArrayList<>();
                String []t=t1.split("\\, ");
                for (String prodName:t) {
                    for (MenuItem m : menu) {
                        if (m.getName().equals(prodName)){
                            orderedProducts.add(m);
                        }
                    }
                }
                r.insertOrder(o, orderedProducts);
                text4.setText("Successful create order");
            }
        });

        generateBill.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
                int id;
                String sId=text1.getText();
                if(sId.equals("")) id=0;
                else id=Integer.parseInt(sId);
                r.generateBill(r.getOrders(), id);
                text4.setText("Generated bill for idOrder="+id);
            }
        });

        showMenu.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
                List<MenuItem> menu = r.getMenu();
                String[] columns= {"id","name","price"};
                String [][]data= new String[50][3];
                int indexMatrice=0;
                for(MenuItem a: menu) {
                    String[] rowElements = new String[3];
                    rowElements[0]=Integer.toString(a.getId());
                    rowElements[1]=a.getName();
                    rowElements[2]=Double.toString(a.ComputePrice());
                    data[indexMatrice]=rowElements;
                    indexMatrice++;
                }
                table = new JTable(data,columns);
                table.setCellSelectionEnabled(true);
                table.setBounds(300,400,500,400);
                JFrame f=new JFrame("Menu");
                f.add(new JScrollPane(table));
                f.setSize(500,400);
                f.setVisible(true);
            }
        });

        showOrders.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
                HashMap<Order, ArrayList<MenuItem>> orders= r.getOrders();
                String[] columns= {"id","table","date", "ordered products","price"};
                String [][]data= new String[30][5];
                int indexMat=0;
                for (Map.Entry m : orders.entrySet()) {
                    String[] rowElements = new String[5];
                    Object obj=m.getKey();
                    Order order=(Order) obj;
                    ArrayList<MenuItem> products=orders.get(order);
                    rowElements[0]=Integer.toString(order.getIdOrder());//id
                    rowElements[1]=Integer.toString(order.getTable());//table
                    Date date=order.getDate();
                    String strDate = dateFormat.format(date);
                    rowElements[2]=strDate;
                    String []prod=new String[10];
                    double totalPrice=0;
                    int size=products.size();
                    for(int i=0;i<size;i++) {
                        prod[i] = products.get(i).getName();
                        totalPrice+=products.get(i).ComputePrice();
                    }
                    String s="";
                    for(String t:prod)
                        if(t!=null)
                          s+=t+", ";
                    rowElements[3]=s;
                    rowElements[4]=Double.toString(totalPrice);
                    data[indexMat]=rowElements;
                    indexMat++;
                }
                table = new JTable(data,columns);
                table.setCellSelectionEnabled(true);
                table.setBounds(300,400,700,400);
                JFrame f=new JFrame("Orders");
                f.add(new JScrollPane(table));
                f.setSize(700,400);
                f.setVisible(true);
            }
        });

        c4.add(text4);
        c4.setLayout(new FlowLayout());

        c5.add(c1);
        c5.add(c2);
        c5.setLayout(new FlowLayout());

        administrator.add(c5);
        administrator.add(c3);
        administrator.add(c4);
        administrator.setLayout(new GridLayout(0,1));
        administrator.setVisible(true);

    }
}
