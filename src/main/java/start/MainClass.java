package start;

import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;
import PresentationLayer.AdministratorGUI;
import PresentationLayer.ChefGUI;
import PresentationLayer.WaiterGUI;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static DataLayer.RestaurantSerializator.Deserialization;
import static DataLayer.RestaurantSerializator.Serialization;

public class MainClass implements Serializable {
    /*
    private static ArrayList<MenuItem> getProducts(String t1, Restaurant r){
        ArrayList<MenuItem> ingredients=new ArrayList<MenuItem>();
        List<MenuItem> menu=r.getMenu();
        String[] t = t1.split("\\, ");
        for (String prodName : t) {
            for (MenuItem m : menu) {
                if (m.getName().equals(prodName)) {
                    ingredients.add(m);
                }
            }
        }
        return ingredients;
    }
    */
    public static void main(String []args){
        /*
        Restaurant r=new Restaurant();
        r.insertBaseProduct("apa", 3);
        r.insertBaseProduct("lamaie", 2);
        r.insertBaseProduct("zahar", 1);
        r.insertBaseProduct("sare", 0.5);
        r.insertBaseProduct("ceapa", 1.5);
        r.insertBaseProduct("morcov", 1);
        r.insertCompositeProduct( "limonada", getProducts("apa, zahar, lamaie", r));
        Serialization(r, "restaurant.ser");
        */
        //Restaurant res=Deserialization("restaurant.ser");
        Restaurant res=Deserialization(args[0]);
        res.existsProducts();
        ChefGUI chef=new ChefGUI(res);
        res.addObserver(chef);
        AdministratorGUI admin=new AdministratorGUI(res);
        WaiterGUI waiter=new WaiterGUI(res);

    }
}
