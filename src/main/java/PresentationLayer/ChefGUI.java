package PresentationLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI implements Observer {

    private Restaurant r;
    private JTextArea text;
    private static String t="";

    public ChefGUI(Restaurant r){
        this.r=r;

        JFrame Chef = new JFrame("Chef");
        text = new JTextArea(20,40);

        Chef.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Chef.setSize(500,400);

        JPanel c1 =new JPanel();
        JScrollPane jsp = new JScrollPane(text);
        c1.add(jsp);
        Chef.add(c1);

        Chef.setVisible(true);
    }

    public void update(Observable o, Object arg) {
        MenuItem item = (MenuItem) arg;
        t+="Prepare "+item.getName()+"\n";
        text.setText(t);
    }
}
