package BusinessLayer;

import java.io.Serializable;
import java.util.*;

import static DataLayer.FileWriter.bill;

/**
 * Aceasta clasa implementeaza diverse functionalitati specifice unui restaurant. Metodele
 * din aceasta clasa vor fi apelate de catre clasele AdministratorGUI si WaiterGUI.
 */

public class Restaurant extends Observable implements Serializable, IRestaurantProcessing {

    private List<MenuItem> menu;
    private HashMap<Order, ArrayList<MenuItem>> orders;
    private static int id=0;
    private static int idO=0;

    /**
     * Constructorul clasei (fara parametrii) care instantiaza obiectele corespunzatoare meniului, respectiv comenzilor
     */
    public Restaurant() {
        this.menu= new ArrayList<>();
        this.orders = new HashMap<>();
    }

    /**
     * metoda care returneaza meniul restaurantului
     * @return meniul restaurantului
     */
    public List<MenuItem> getMenu() { return menu; }

    /**
     * metoda care returneaza comenzile procesate in cadrul restaurantului
     * @return comenzile
     */
    public HashMap<Order, ArrayList<MenuItem>> getOrders() { return orders; }

    /**
     * metoda care returneaza id ul maim al produselor din meniu
     * @param id id ul curent al produsului
     * @return id ul maxim
     */
    private int maxim(int id){
        for(MenuItem m: menu)
            if(m.getId()>id)
                id=m.getId();
        return id;
    }

    /**
     * Aceasta metoda se ocupa cu inserarea unui BaseProdus in meniu, urmand sa fie apelata
     * de catre administrator din cadrul ferestrei corespunzatoare.
     * @param name numele produsului
     * @param price pretul produsului
     * @return mesaj de succes sau de eroare
     */
    public String insertBaseProduct(String name, double price){
        assert !name.equals(""): "Nume invalid";
        assert price!=0: "Pretul este egal cu 0";
        id=maxim(id);
        id++;
        for(MenuItem product: menu) {
            if (product.getName().equals(name)) {
                return "Insertion failed";
            }
        }
        menu.add(new BaseProduct(id, name, price));
        return "Successful insertion";
    }

    /**
     * Aceasta metoda se ocupa cu inserarea unui CompositeProdus in meniu, urmand sa fie apelata
     * de catre administrator din cadrul ferestrei corespunzatoare.
     * @param name numele produsului
     * @param ingredients ingredientele pe care le contine acest produs
     * @return mesaj de succes sau de eroare
     */
    public String insertCompositeProduct(String name, ArrayList<MenuItem> ingredients) {
        assert !name.equals(""): "Nume invalid";
        assert !ingredients.isEmpty(): "Lista de ingrediente este goala";
        id=maxim(id);
        id++;
        for(MenuItem product: menu) {
            if (product.getName().equals(name)) {
                return "Insertion failed";
            }
        }
        CompositeProduct comp=new CompositeProduct(id, name);
        comp.setIngredients(ingredients);
        comp.ComputePrice();
        menu.add(comp);
        return "Successful insertion";
    }

    /**
     * Aceata metoda se ocupa cu stergerea unui produs din meniu, fiind apelata de administrator
     * in cadrul ferestrei corespunzatoare. Odata cu stergerea produslui din meniu se vor sterge
     * si toate produsele care-l contin.
     * @param name numele produsului care urmeaza a fi strers
     * @return mesaj de succes sau de eroare
     */
    public String deleteProduct(String name){
        assert !menu.isEmpty(): "Nu exista produse in meniu";
        assert !name.equals(""): "Nume invalid";
        int index=-1;
        String nameP="";
        for(MenuItem product: menu) {
            if (product.getName().equals(name))
                index = menu.indexOf(product);
        }
        if(index!=-1)
            menu.remove(index);
        for(MenuItem product: menu) {
            if (product instanceof CompositeProduct) {
                for (MenuItem m : ((CompositeProduct) product).getIngredients()) {
                    if (m.getName().equals(name)) {
                        nameP = product.getName();
                    }
                }
            }
        }
        if(!nameP.equals(""))
            deleteProduct(nameP);
        if(index==-1)
            return "Deletion failed";
        else
            return "Successful deletion";
    }

    /**
     * Metoda care se ocupa cu actualizare unui anumit produs specificat de metoda updateProduct
     * @param id id ul prodului
     * @param name noul nume al produsului
     * @param price noul pret al produslui
     * @param ingredients noua lista de ingrediente a produsului
     * @param m produsl
     */
    private void update(int id, String name, double price, ArrayList<MenuItem> ingredients, MenuItem m){
        if (m.getId() == id) {
            if (m instanceof BaseProduct) {
                if (!name.equals("")) {
                    ((BaseProduct) m).setName(name);
                }
                if (price != 0) {
                    ((BaseProduct) m).setPrice(price);
                }
            } else if (m instanceof CompositeProduct) {
                if (!name.equals("")) {
                    ((CompositeProduct) m).setName(name);
                }
                if (ingredients != null) {
                    ((CompositeProduct) m).setIngredients(ingredients);
                    m.ComputePrice();
                }
            }
        }
    }

    /**
     * Aceasta metoda se ocupa cu actualizarea unui produs specificat in cadrul interfetei prin id ul acestuia.
     * Actualizarea se face in functie de campurile completate in interfata, daca un anumit camp nu este completat el
     * nu se va actualiza, ci va ramane la fel ca inaite. Modificare unui produs se va face si in toate celelate produse
     * care il contin.
     * @param id id ul prodului
     * @param name noul nume al produsului
     * @param price noul pret al produslui
     * @param ingredients noua lista de ingrediente a produsului
     */
    public void updateProduct(int id, String name, double price, ArrayList<MenuItem> ingredients){
        assert !menu.isEmpty(): "Nu exista produse in meniu";
        assert id!=0: "Id-ul nu este valid";
        for(MenuItem product: menu) {
            update(id,name, price, ingredients, product);
        }
        for(MenuItem product: menu) {
            if (product instanceof CompositeProduct) {
                for (MenuItem m : ((CompositeProduct) product).getIngredients()) {
                    update(id, name, price, ingredients, m);
                }
            }
        }
    }

    /**
     * Aceasta metoda se ocupa cu inserarea unei noi comenzi si notificarea chef-ului de fiecare data cand
     * in lista de produse comandate se gaseste un produs compus. Aceasta metoda va fi apelata de waiter,
     * in cadrul ferestrei corespunzatoare.
     * @param o comanda care urmeaza a fi inserata
     * @param orderProducts produsele comandate
     */
    public void insertOrder(Order o,ArrayList<MenuItem> orderProducts) {
        assert !orderProducts.isEmpty(): "Lista de produse comandate nu contine nici un produs";
        assert o!=null: "Comanda nu exista";
        idO++;
        for(MenuItem m: orderProducts){
            if(menu.contains(m))
                orders.put(o, orderProducts);
        }
        for(MenuItem m: orderProducts) {
            if (m instanceof CompositeProduct){
                setChanged();
                notifyObservers(m);
            }
        }
    }

    /**
     * Aceasta metoda are rolul de a calcula pretul comenzii si de genera nota de plata pentru o anumita
     * comanda a carei id este primit ca parametru, apeland metoda bill din clasa FileWriter
     * @param orders lista de comenzi
     * @param idOrder id ul comenzii
     */
    public void generateBill(HashMap<Order, ArrayList<MenuItem>> orders, int idOrder){
         assert !orders.isEmpty(): "Lista de comenzi este goala";
         assert idOrder!=0: "Id ul este invalid";
         bill(orders, idOrder);
    }

    /**
     * Metoda care testeaza daca se afla produse in meniu la lansarea aplicatei
     */
    public void existsProducts() {
        assert !menu.isEmpty(): "Nu exista produse in meniu";
    }

    /**
     * Metoda care verifica daca s-au efectuat comenzi in restaurant
     */
    public void existsOrders() { assert idO!=0: "Nu s-a efectuat nici o comanda";
    }
}
