package BusinessLayer;

import java.util.ArrayList;
import java.util.HashMap;

public interface IRestaurantProcessing {

    /**
     * trebuie sa existe produse in meniul restaurantului la lansarea aplicatiei
     *
     * @pre !menu.isEmpty()
     */
    void existsProducts();

    /**
     * trebuie sa existe comenzi dupa ce se lucreaza cu aplicatia
     *
     * @post nrOrders!=0
     */
    void existsOrders();

    /**
     * numele si pretul trebuie sa fie nenule
     *
     * @param name  numele produsului
     * @param price pretul produsului
     * @return mesaj de succes sau de eroare
     * @pre name!=null
     * @pre price!=0
     */
    String insertBaseProduct(String name, double price);

    /**
     * numele si lista de ingrediente trebuie sa fie nenule
     *
     * @param name        numele produsului
     * @param ingredients ingredientele pe care le contine acest produs
     * @return mesaj de succes sau de eroare
     * @pre name!=null
     * @invariant !ingredients.isEmpty()
     */
    String insertCompositeProduct(String name, ArrayList<MenuItem> ingredients);

    /**
     * numele nu trebuie sa fie null si meniul sa contina produse
     *
     * @param name numele produsului care urmeaza a fi strers
     * @return mesaj de succes sau de eroare
     * @pre !menu.isEmpty()
     * @pre name!=null
     */
    String deleteProduct(String name);

    /**
     * id ul trebuie sa fie diferit de 0 si meniul sa contina produse
     * @param id          id ul prodului
     * @param name        noul nume al produsului
     * @param price       noul pret al produslui
     * @param ingredients noua lista de ingrediente a produsului
     * @invariant !menu.isEmpty()
     * @pre id!=0
     */
    void updateProduct(int id, String name, double price, ArrayList<MenuItem> ingredients);

    /**
     * comanda nu trebuie sa fie nula si trebuie sa existe o lista de produse comandate care sa nu fie nula
     * @param o             comanda care urmeaza a fi inserata
     * @param orderProducts produsele comandate
     * @pre o!=null
     * @invariant !orderProducts.isEmpty()
     */
    void insertOrder(Order o, ArrayList<MenuItem> orderProducts);

    /**
     * lista de comenzi nu trebuie sa fie goala si id ul comenzii trebuie s fie valid (diferit de 0)
     * @param orders  lista de comenzi
     * @param idOrder id ul comenzii
     * @pre !orders.isEmpty()
     * @pre idO!=0
     */
    void generateBill(HashMap<Order, ArrayList<MenuItem>> orders, int idOrder);
}